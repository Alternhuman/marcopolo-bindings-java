package net.marcopolo.binding;

public class PoloInternalException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 36135900294986250L;

	public PoloInternalException(String msg){
		super(msg);
	}
}
