package net.marcopolo.binding;

import java.util.HashMap;

public class Node {
	private String address;
	private HashMap<String, Parameter> params;
	
	public HashMap<String, Parameter> getParams() {
		return params;
	}
	public void setParams(HashMap<String, Parameter> params) {
		this.params = params;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
