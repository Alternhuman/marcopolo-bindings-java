package net.marcopolo.binding;

public class PoloException extends Exception{

	public PoloException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -860519340467123187L;

}
